@echo off
cd /d %~dp2 && md tmp || pause && exit /b
echo Saving as "%~dp2%~n1 + %~n2.ttf"
echo Please wait
"%~dp0bin\otfccdump.exe" --ignore-hints -o tmp\base.otd %1
"%~dp0bin\otfccdump.exe" --ignore-hints -o tmp\ext.otd %2
"%~dp0bin\mergeotd.exe" tmp\base.otd tmp\ext.otd
"%~dp0bin\otfccbuild.exe" -q -O3 -o "%~n1 + %~n2.ttf" tmp\base.otd
rmdir /s /q tmp
echo Completed
pause
